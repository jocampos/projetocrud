<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('usuarios');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/usuarios', 'UsuariosController@request')->name('usuarios');
Route::get('/inativos', 'UsuariosController@inativos')->name('inativos');
Route::post('/usuarios', 'UsuariosController@create')->name('cadUsuario');
Route::get('/usuarios/novo', 'UsuariosController@novo')->name('novoUsuario');
Route::get('/usuarios/delete/{cpf}', 'UsuariosController@delete')->name('delete');
Route::get('/usuarios/inativar/{cpf}', 'UsuariosController@inativar')->name('inativar');
Route::get('/usuarios/reativar/{cpf}', 'UsuariosController@reativar')->name('reativar');
Route::get('/usuarios/editar/{cpf}', 'UsuariosController@editar')->name('editarUsuario');
Route::put('/usuarios/editar', 'UsuariosController@update')->name('updateUsuario');
