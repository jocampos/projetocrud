<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use \GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;

class UsuariosController extends Controller
{
    //

    public function index(){
        
    }

    public function novo(){
        return view('novoUsuario');
    }

    public function create(Request $request){
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users|max:100',
            'birthday' => 'required|date',
            'password' => 'required',
            'cpf'=>'required|max:14|min:14',
            'foto'=>'required'
        ]);

        $usuario = $request->except('foto');
        $img = $this->getImg($request->file('foto'));

        if(empty($img->img)){
            return redirect()->back()->withErrors('A imagem não pode ser enviada, tente outra imagem.')->withInput();
        }

        $usuario['image'] = $img->img;
        $usuario['password'] =  bcrypt($usuario['password']);
        $user = new User();
        $user->create($usuario);
        return redirect()->route('usuarios');
    }

    public function request(){
        $user = new User();
        $usuarios = $user->paginate(5);

        $titulo = "Usuários Ativos";

        return view('usuarios', compact('usuarios', 'titulo'));
    }

    public function inativos(){
        $user = new User();
        $usuarios = $user->onlyTrashed()->paginate(5);
        $titulo = "Usuários Inativos";

        return view('usuarios', compact('usuarios', 'titulo'));
    }

    public function editar($cpf){

        if(empty($cpf))
            return redirect()->back()->withErrors('Erro ao editar.')->withInput();

        $user = new User();

        $usuario = $user->where('cpf', $cpf)->first();

        return view('editarUsuario', compact('usuario'));
    }

    public function update(Request $request){
        $this->validate($request, [
            'name' => 'required|max:255',
            'birthday' => 'required|date'
        ]);

        $user = new User();
        $edit = $user->where('cpf', $request->cpf)->first();

        $usuario = $request->except(['foto', 'cpf', 'email']);

        if(!empty($request->password))
            $usuario['password'] =  bcrypt($usuario['password']);
        else
            $usuario = $request->except(['foto', 'cpf', 'email', 'password']);


        if($request->file('foto')){
            if(!$this->deleteImg($edit->getAttribute('image'))){
                return redirect()->back()->withErrors('Erro ao deletar.')->withInput();
            }
            $img = $this->getImg($request->file('foto'));
            $usuario['image'] = $img->img;
        }


        $edit->update($usuario);
        return redirect()->route('usuarios');
    }

    public function delete(Request $request){
        $user = new User();
        $usuario = $user->onlyTrashed()->where('cpf', $request->cpf)->first();

        if(!$this->deleteImg($usuario->getAttribute('image'))){
            return redirect()->back()->withErrors('Erro ao deletar.')->withInput();
        }

        $usuario->forceDelete();
        return redirect()->route('inativos');
    }

    public function inativar(Request $request){
        $user = new User();
        $usuario = $user->where('cpf', $request->cpf)->first();

        $usuario->delete();
        return redirect()->route('usuarios');
    }

    public function reativar(Request $request){
        $user = new User();
        $usuario = $user->onlyTrashed()->where('cpf', $request->cpf)->first();

        $usuario->restore();
        return redirect()->route('inativos');
    }

    private function deleteImg($imagem){
        $client = New Client();
        $response = $client->request('DELETE', env('API').'/api/deletar',[
            'form_params' => [
                'foto' => $imagem
            ],
        ]);
        if($response->getStatusCode() == 200)
            return json_decode($response->getBody());
        else
            return false;
    }

    private function getImg($imagem){

        $client = new Client();
        $response = $client->request('POST',env('API').'/api/upload', [
            //'form_params' => $imagem,
            'multipart' => [
                [
                    'name'     => 'foto',
                    'contents' => file_get_contents($imagem),
                    'filename' => $imagem
                ]
            ]
        ]);

        return json_decode($response->getBody());
    }
}
