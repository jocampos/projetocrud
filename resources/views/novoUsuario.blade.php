@extends('layouts.usuarios')

@section('conteudo')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            @include('layouts.ativoInativo')
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <painel titulo="Novo Usuário">
                @if($errors->any())
                    <h4 class="alert alert-danger">{{$errors->first()}}</h4>
                @endif

                <form id="formulario" action="{{route('cadUsuario')}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                <input type="text" name="name" value="{{ old('name') }}" placeholder="Nome" class="form-control" required>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <input type="email" placeholder="E-mail" name="email" class="form-control" value="{{ old('email') }}" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('birthday') ? ' has-error' : '' }}">
                                <input type="date" name="birthday" placeholder="Data de nascimento"  value="{{ old('birthday') }}"  class="form-control" required>
                                @if ($errors->has('birthday'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('birthday') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group cpf {{ $errors->has('cpf') ? ' has-error' : '' }}">
                                <input type="text" name="cpf" placeholder="CPF" onkeydown="javascript: fMasc(this, mCPF);" class="form-control" value="{{ old('cpf') }}" required>
                                @if ($errors->has('cpf'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('cpf') }}</strong>
                                    </span>
                                @endif
                                    <span class="cpfhelp help-block">
                                        <strong>CPF inválido.</strong>
                                    </span>
                            </div>
                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }} senha">
                                <input type="password" name="password" placeholder="Senha"  class="form-control" value="{{ old('password') }}" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            
                            <div class="for-group senha">
                                <input type="password" name="validasenha" placeholder="Confirme a senha" class="form-control">
                                <br>
                            </div>
                            
                            <div class="form-group {{ $errors->has('foto') ? ' has-error' : '' }}">
                                <input id="foto" type="file" name="foto" placeholder="Foto" class="form-control" value="{{ old('foto') }}" required>
                                @if ($errors->has('foto'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('foto') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success" type="submit">Enviar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </painel>
        </div>
    </div>

@endsection

@section('scripts')
<script>
    function validasenha() {
       if($('input[name="password"]').val() !== $('input[name="validasenha"]').val()){
           $('.senha').addClass('has-error')
           return false
       }else{
           $('.senha').removeClass('has-error')
           return true
       }

    }

    $('input[name="password"]').on('blur', function(){
        validasenha()
    })

    $('input[name="validasenha"]').on('keyup', function(){
        validasenha()
    })

    $('input[name="cpf"]').on('blur', function(){
        var cpf = $(this).val().replace('.', '').replace('.', '').replace('-', '')
        if(!TestaCPF(cpf)){
            $('.cpf').addClass('has-error')
            $('.cpfhelp').show()
        }else{
            $('.cpf').removeClass('has-error')
            $('.cpfhelp').hide()
        }
    })

    $('#formulario').submit(function (e) {
        e.preventDefault()
        $('.overlay').show()
        var cpf = $('input[name="cpf"]').val().replace('.', '').replace('.', '').replace('-', '')

        if(validasenha() && TestaCPF(cpf)){
            this.submit()
        }else{
            $('.overlay').hide()
            alert('A confirmação da senha deve ser igual');
        }
    })

    function TestaCPF(strCPF) {
        var Soma;
        var Resto;
        Soma = 0;
        if (strCPF == "00000000000") return false;

        for (i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
        Resto = (Soma * 10) % 11;

        if ((Resto == 10) || (Resto == 11))  Resto = 0;
        if (Resto != parseInt(strCPF.substring(9, 10)) ) return false;

        Soma = 0;
        for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
        Resto = (Soma * 10) % 11;

        if ((Resto == 10) || (Resto == 11))  Resto = 0;
        if (Resto != parseInt(strCPF.substring(10, 11) ) ) return false;
        return true;
    }


    function fMasc(objeto,mascara) {
        obj=objeto
        masc=mascara
        setTimeout("fMascEx()",1)
    }
    function fMascEx() {
        obj.value=masc(obj.value)
    }
    function mCPF(cpf){
        cpf=cpf.replace(/\D/g,"")
        cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2")
        cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2")
        cpf=cpf.replace(/(\d{3})(\d{1,2})$/,"$1-$2")
        return cpf
    }
</script>
@endsection