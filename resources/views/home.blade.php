@extends('layouts.usuarios')

@section('conteudo')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <card v-bind:item="{{ json_encode(Auth::user()) }}" bg="{{url('img/bg.jpg')}}" rota="{{route('usuarios')}}"></card>
        </div>
    </div>
</div>
@endsection
