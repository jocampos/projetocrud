<div class="btn-group btn-group-justified" role="group" aria-label="...">
    <div class="btn-group" role="group">
        <a href="{{route('usuarios')}}" type="button" class="btn btn-success">Usuários Ativos</a>
    </div>
    <div class="btn-group" role="group">
        <a href="{{route('inativos')}}" type="button" class="btn btn-danger">Usuários Inativos</a>
    </div>
</div>
<br>