@extends('layouts.usuarios')
@section('conteudo')
    @include('layouts.ativoInativo')
<div class="row">
    <div class="col-md-12">
        <painel titulo="{{$titulo}}">
            @if($errors->any())
                <h4 class="alert alert-danger">{{$errors->first()}}</h4>
            @endif
            <tabela-lista :items="{{json_encode($usuarios)}}" rota="{{route('usuarios')}}" novoreg="{{route('novoUsuario')}}"></tabela-lista>

            {{ $usuarios->links() }}
        </painel>
    </div>
</div>

@endsection